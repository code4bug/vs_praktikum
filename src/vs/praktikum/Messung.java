package vs.praktikum;

import java.util.Date;

public class Messung {
    private Ware ware;
    private int value;
    private Date time;
    
    public Messung(Ware ware, int value, Date time) {
        this.ware = ware;
        this.value = value;
        this.time = time;
        /* TODO: Date wegen Java 7 lassen oder ändern? (auf Mac-VMs: Java 1.7) */
    }

    public Ware getWare() {
        return ware;
    }

    public void setWare(Ware ware) {
        this.ware = ware;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
    
    
}
