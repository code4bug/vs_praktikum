package vs.praktikum.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class HTTPTests {
    //private static final String SERVER_ADRESS = "192.168.178.64";
    private static final String SERVER_ADRESS = "localhost";
    private static final int SERVER_PORT = 4455;
    
    // Anzahl der HTTP-Anfragen, die durchgeführt werden sollen
    private static final int REQUEST_COUNT = 10000;
    
    public static void main(String[] args) {
        // Zählt die Anzahl der erfolgreichen HTTP-Anfragen (Es gab eine gültige Antwort des Servers)
            int successCounter = 0;
        
        try {
            String body = "GET / HTTP/1.1\r\n\r\n";
            long startTime = System.currentTimeMillis();
            
            for(int i = 0; i < REQUEST_COUNT; i++) {
                Socket clientSocket = new Socket(SERVER_ADRESS, SERVER_PORT);
                
                DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
                
                //DataInputStream inFromServer = new DataInputStream(clientSocket.getInputStream());
                
                BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                
                outToServer.writeBytes(body);
                outToServer.flush();
                
                String line;
                
                while((line = reader.readLine()) != null) {
                    if(line.equals("HTTP/1.1 200 OK"))
                        successCounter++;
                }
                
                clientSocket.close();
            }
            
            long time = (System.currentTimeMillis() - startTime);
            
            System.out.println("Erfolgreiche Anfragen: " + successCounter + " von " + REQUEST_COUNT);
            System.out.println("Vergangene Zeit: " + time + " ms");
            
        } catch(Exception e) {
            System.out.println(successCounter + ": Verbindung fehlgeschlagen: " + e.getLocalizedMessage());
        }
        
    }
}