package vs.praktikum;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Sensor {    
    
    private String name;
    private int fuellstand;
    private String serverAdress;
    private int serverPort;
    private InetAddress ia;
    
    public Sensor(String name, int fuellstand, String serverAdress, int serverPort) throws UnknownHostException {
        this.name = name;
        this.fuellstand = fuellstand;
        this.serverAdress = serverAdress;
        this.serverPort = serverPort;
        
        ia = InetAddress.getByName( this.serverAdress );
    }
    
    /**
     * Sendet aktuellen Füllstand an Zentrale
     * @throws Exception 
     */
    public void sendToServer() throws Exception {
        // Gesendeter Datenstrom hat Format: Produktname:Füllstand
        String s = this.name + ":" + this.fuellstand;
        byte[] raw = s.getBytes();
        DatagramPacket packet = new DatagramPacket( raw, raw.length, ia, this.serverPort );
        //System.out.println(packet.getSocketAddress());
        
        // Paket an Server senden
        DatagramSocket socket = new DatagramSocket();
        socket.setSoTimeout(1000);
        if(socket.isConnected()) System.out.println("Connected");
        socket.send( packet );
        
        // Auf Antwort des Servers warten
        DatagramPacket responsePacket = new DatagramPacket( new byte[1024], 1024 );
        socket.receive( responsePacket );
        
        // Antworttext zu String umwandeln
        String responseText = new String( responsePacket.getData(), 0, responsePacket.getLength() );
        
        if(!s.equals(responseText.trim()))
            throw new Exception("Paket wurde vom Server nicht akzeptiert: " + responseText);
        else
            System.out.println( "Gesendet: " + s );
    }
    
    /**
     * Reduziert Füllstand des Sensors um 1 und sendet neuen Füllstand an Zentrale
     * @throws Exception 
     */
    public void down() throws Exception {
        if(fuellstand > 0) {
            fuellstand--;
            sendToServer();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFuellstand() {
        return fuellstand;
    }

    public void setFuellstand(int fuellstand) throws Exception {
        this.fuellstand = fuellstand;
        sendToServer();
    }
    
    public String getServerAdress() {
        return serverAdress;
    }

    public void setServerAdress(String serverAdress) {
        this.serverAdress = serverAdress;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }    
    
}
