package vs.praktikum;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Zentrale {

    public static final int HTTP_SERVER_PORT = 4455;
    public static final int SENSOR_SERVER_PORT = 4466;
    
    public static void main(String[] args) throws IOException {        
        System.out.println("Kühlschrank-Zentrale");
        System.out.println("----------------------");
        
        List<Ware> waren = new ArrayList();
        List<Messung> historie = new ArrayList();

        // HTTP-Server in eigenem Thread starten
        Thread t1 = new HTTPServer(4455, waren, historie);
        t1.start();
        
        // Server, der Füllstände der Sensoren empfängt, in eigenem Thread starten
        Thread t2 = new SensorServer(4466, waren, historie);
        t2.start();
    }  
}
