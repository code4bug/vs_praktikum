package vs.praktikum;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Date;
import java.util.List;

public class SensorServer extends Thread {
    
    private final int port;
    private final List<Ware> waren;
    private final List<Messung> historie;
    
    public SensorServer(int port, List<Ware> waren, List<Messung> historie) {
        super();
        this.port = port;
        this.waren = waren;
        this.historie = historie;
    }
    
    @Override
    public void run() {
        DatagramSocket socket;
        try {
            socket = new DatagramSocket( port );
 
            System.out.println("Sensoren: Verbindung über folgenden Port aufbauen: " + port);

            while ( true )
            {
                // Auf Anfrage warten
                DatagramPacket packet = new DatagramPacket( new byte[1024], 1024 );
                socket.receive( packet );

                // Empfänger auslesen
                InetAddress address = packet.getAddress();
                int port = packet.getPort();
                int len = packet.getLength();
                byte[] data = packet.getData();

                String sensorData = new String( data, 0, len );
                String[] sensorDataParts = sensorData.split(":");

                // Prüfen ob Name und Füllstand existieren, und ob Füllstand numerisch und nicht negativ ist
                if(sensorDataParts.length == 2 && sensorDataParts[1].matches("^\\d+$")) {
                    String product = sensorDataParts[0];
                    int value = Integer.valueOf(sensorDataParts[1]);

                    Ware w = new Ware(product, value);
                    
                    if(!waren.contains(w)) {
                        waren.add(w);
                        System.out.println("Neuer Sensor \"" + product + "\" [" + address + ":" + port + "] meldet Füllstand " + value);
                        historie.add(new Messung(w, value, new Date()));
                    }
                    else {
                        for(int i=0; i<waren.size(); i++) {
                            if(waren.get(i).getName().equals(product)) {
                                waren.get(i).setValue(value);
                                System.out.println("Sensor \"" + product + "\" [" + address + ":" + port + "] meldet Füllstand " + value);
                                historie.add(new Messung(waren.get(i), value, new Date()));
                            }
                        }
                    }
                    
                    // Dem Client antworten (selber String wie gesendet)
                    DatagramPacket responsePacket = new DatagramPacket(data , data.length , packet.getAddress() , packet.getPort());
                    socket.send(responsePacket);
                } else {
                    // Dem Client mit Fehlermeldung antworten
                    String errorMsg = "Ungueltiges Paket";
                    DatagramPacket responsePacket = new DatagramPacket(errorMsg.getBytes() , errorMsg.getBytes().length , packet.getAddress() , packet.getPort());
                    socket.send(responsePacket);
                    System.out.println("- Ungueltiges Paket erhalten von " + address + ":" + port);
                }
            }
        
        } catch (IOException | NumberFormatException ex) {
            System.out.println(ex.getMessage());
        }
        
    }
}
