package vs.praktikum;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.regex.Pattern;

public class HTTPServer extends Thread {
    
    private final int port;
    private final List<Ware> waren;
    private final List<Messung> historie;
    
    public HTTPServer(int port, List<Ware> waren, List<Messung> historie) {
        super();
        this.port = port;
        this.waren = waren;
        this.historie = historie;
    }
    
    @Override
    public void run() {
        ServerSocket serverSocket = null;
        Socket conSocket = null;
        
        try {
            serverSocket = new ServerSocket(port);
            while(true) {
                System.out.println("Warte auf Anfrage");
                conSocket = serverSocket.accept();
                
                InputStream inputStream = conSocket.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                
                // Anfrage verarbeiten (Prüfen ob HTTP-Header existiert)
                String line = null;
                boolean httpHeaderFound = false;
                while(!(line = reader.readLine()).trim().equals("")) { 
                    if(Pattern.matches("GET .* HTTP/[0-9.]*", line.trim()))
                        httpHeaderFound = true;
                    System.out.println(line);
                }
                if(!httpHeaderFound) {
                    System.out.println("\nUngültige Anfrage von " + conSocket.getInetAddress() + ":" + conSocket.getPort());
                    break;
                }
                
                System.out.println("Anfrage erhalten von " + conSocket.getInetAddress() + ":" + conSocket.getPort());

                // Antwort an Client
                DataOutputStream outputStream = new DataOutputStream(conSocket.getOutputStream());
                
                // HTML Body generieren
                String body = "<html><body>"
                        + "<style type=\"text/css\">"
                        + "body { font-family: Arial, sans-serif; }"
                        + "table, th, td { border: 1px solid; }"
                        + "td { padding: 3px 5px; }"
                        + "</style>"
                        + "  <h1>K&uuml;hlschrank-Zentrale</h1>"
                        + "  <h2>Waren-F&uuml;llstand:</h2>";
                
                // Wenn Waren vorhanden sind, alle Waren mit Füllständen ausgeben
                if(waren.isEmpty())
                    body += "<p>Keine Daten vorhanden.</p>";
                else {
                    for(int i=0; i < waren.size(); i++) {
                        body += "  <p><b>" + waren.get(i).getName() + ":</b> " + waren.get(i).getValue() + "</p>";
                    }
                }
                
                body += "  <h2>Historie:</h2>";
                
                // Wenn Daten in der Historie vorhanden sind, Historie in Tabelle ausgeben
                if(historie.isEmpty())
                    body += "<p>Keine Daten vorhanden.</p>";
                else {
                    body += "  <table>"
                            + "    <th>Zeit</th> <th>Ware</th> <th>F&uuml;llstand</th>";
                    for(int i=0; i < historie.size(); i++) {
                        body += "    <tr><td>" + historie.get(i).getTime().toLocaleString() + "</td> <td>" 
                                + historie.get(i).getWare().getName() + "</td> <td>" 
                                + historie.get(i).getValue() + "</td></tr>";
                    }
                    body += "  </table>";
                }
                body += "</body></html>";
                
                // HTTP Header und Body an Client senden
                outputStream.writeBytes("HTTP/1.1 200 OK\r\n"
                                        + "Content-Length: "+ body.getBytes().length + "\r\n"
                                        + "Content-Type: text/html\r\n\r\n"
                                        + body);
                outputStream.close();
                conSocket.close();
            }
            //serverSocket.close();
        } catch(IOException ex) {
            System.out.println(ex.getMessage());
        } finally {
            
        }
        
    }
}
