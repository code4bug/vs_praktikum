package vs.praktikum;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Client {
    private static final String SERVER_ADRESS = "192.168.178.64";
    private static final int SERVER_PORT = 4466;
    
    public static void main(String[] args) throws Exception {
        List<Sensor> sensors = new ArrayList();
        
        Scanner reader = new Scanner(System.in);
        
        // Eingabe Produktname
        for(int i=1; i<=4; i++) {
            String name = "";
            while(name.equals("")) {
                System.out.println("Name des " + i + ". Sensors: ");
                name = reader.next();
            }

            // Eingabe Anfangsbestand
            int value = 0;
            while(value == 0) {
                System.out.println("Initialer Fuellstand des " + i + ". Sensors: ");
                if(!reader.hasNextInt())
                    reader.next();
                else {
                    int val = reader.nextInt();
                    if(val >= 0)
                        value = val;
                }  
            }
            
            try {
                sensors.add(new Sensor(name, value, SERVER_ADRESS, SERVER_PORT));
            } catch (UnknownHostException ex) {
                System.out.println("Verbindungsaufbau fehlgeschlagen! Grund: " + ex.getLocalizedMessage());
                return;
            }
        }
        
            
        while (true) {
            for(int i=0; i<sensors.size(); i++) {
                try {
                    // Füllstand des Sensors um 1 reduzieren
                    sensors.get(i).down();

                    // Wenn ein Sensor den Füllstand 0 erreicht hat, wird der Füllstand nicht weiter reduziert
                    if(sensors.get(i).getFuellstand() <= 0) {
                        sensors.remove(i);
                        i--;
                    }
                } catch (Exception ex) {
                    System.out.println("Die Kommunikation des Sensors '" + sensors.get(i).getName() + "' ist fehlgeschlagen! Grund: " + ex.getLocalizedMessage());
                    sensors.remove(i);
                    i--;
                }
            }

            System.out.println("-------------");

            // Ende, wenn alle Sensoren Füllstand 0 erreicht haben
            if(sensors.size() <= 0)
                break;

            // Füllstand aller Sensoren wird ein Mal pro Sekunde reduziert
            Thread.sleep( 1000 );
        }
    }
}
